<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item ">
            <a href="{{ route('admin.home')}}" class="nav-link {{Request::is('admin') ? 'active' : ''}} ">
                <i class="nav-icon fas fa-home " ></i>
              <p> Home </p>
            </a>
          </li>
        <li class="nav-item has-treeview {{Request::is('admin/posts*') ? 'menu-open' : ''}}">
        <a href="#" class="nav-link {{Request::is('admin/posts') ? 'active' : ''}}">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p> Blog <i class="right fas fa-angle-left"></i> </p>
        </a>
        <ul class="nav nav-treeview active">
          <li class="nav-item">
            <a href="{{ route('admin.posts.index')}}" class="nav-link {{Request::is('admin/posts') ? 'active' : ''}}" >
              <i class="nav-icon fas fa-eye"></i>
              <p>Ver todos los posts</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin.posts.create')}}" class="nav-link {{Request::is('admin/posts/create') ? 'active' : ''}}">
                <i class="nav-icon fas fa-edit"></i>
              <p>Crear un post</p>
            </a>
          </li>
        </ul>
      </li>

    </ul>
  </nav>
