{{-- extiende la plantilla layout --}}
@extends('admin.layout')

{{-- codigo que vamos a inyevtar en el yield del admin.layout --}}
@section("header")
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Inicio</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">Inicio</li>
            <li class="breadcrumb-item active"><a href="{{ route('admin.posts.index')}}">Blog</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@stop

@section("content")
    <h1>Dashboard</h1>
    <p>Usuario authenticado: {{auth()->user()->name}}</p>
@stop
