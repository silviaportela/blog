{{-- extiende la plantilla layout --}}
@extends('admin.layout')

@section("header")
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h3 class="m-0 text-dark">Crear Post</h3>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active"><a href="{{ route('admin.home')}}"><i
                                class="nav-icon fas fa-tachometer-alt"></i> Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="{{ route('admin.posts.index')}}"><i
                                class="nav-icon fas fa-list"></i> Blog </a></li>
                    <li class="breadcrumb-item active">Crear Post</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@stop


{{-- codigo que vamos a inyevtar en el yield del admin.layout --}}
@section("content")
<form>
    <div class="row">
        <div class="col-md-8">
            <form>
                <div class="box box-primary">
                    <div class="box-body form-group">
                        <label>Titulo de la publicación</label>
                        <input type="text" class="form-control" name="title"
                            placeholder="ingresa aqui el nombre de la publicacion">
                    </div>
                    <div class="box-body form-group">
                        <label>Contenido</label>
                        <textarea rows="10" name="body" class="form-control"
                            placeholder="ingresa el contenido completo de la publicacion"></textarea>
                    </div>
                </div>
        </div>

        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        <label>Fecha de publicacion:</label>
                        <div class="input-group date">
                            <input type="text" class="form-control datetimepicker-input"
                                data-target="#reservationdate" />
                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Extracto</label>
                        <textarea name="excerpt" class="form-control"
                            placeholder="ingresa aqui el extracto de la publicacion"></textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>
@stop 

$('#reservationdate').datetimepicker({
    format: 'L'
});