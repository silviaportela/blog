{{-- extiende la plantilla layout --}}
@extends('admin.layout')

@section("header")
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
{{-- <h1 class="m-0 text-dark">Blog</h1> --}}
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active"><a href="{{ route('admin.home')}}"><i class="nav-icon fas fa-tachometer-alt"></i> Inicio</a></li>
            <li class="breadcrumb-item active"><a href="{{ route('admin.posts.index')}}"><i class="nav-icon fas fa-list"></i> Blog </a></li>
            <li class="breadcrumb-item active">Ver posts</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  
@stop


{{-- codigo que vamos a inyevtar en el yield del admin.layout --}}
@section("content")
<div class="card">
    <div class="card-header">
      <h3 class="card-title">DataTable with default features</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="posts-table" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>ID</th>
          <th>Titulo</th>
          <th>Extracto</th>
          <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            @foreach ( $posts as $post )
                <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->excerpt }}</td>
                <td>
                    <a href="#" class="btn btn-xs btn-info"><i class="fa fa-pen"></i></a>
                    <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                </td>
                </tr>
            @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>

  
@stop
