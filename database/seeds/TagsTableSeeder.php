<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::truncate(); //limpia la tabla post

        $post = new Tag;
        $post->name = "Viajes";
        $post->save();

        $post = new Tag;
        $post->name = "Recetas";
        $post->save();


    }
}
