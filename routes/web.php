<?php

use Illuminate\Support\Facades\Route;

Route::get('/','PagesController@index');

Route::group([
    'prefix'=>'admin',
    'namespace'=>'Admin',
    'middleware'=>'auth'],
    function(){
        Route::get('/', 'AdminController@index')->name('admin.home');
        Route::get('posts', 'PostsController@index')->name('admin.posts.index');
        Route::get('posts/create', 'PostsController@create')->name('admin.posts.create');
    }
);


Auth::routes(['register' => false]);


