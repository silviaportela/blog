<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Sobre la aplicación

Blog + panel de administración del blog.

Este blog desarrollado en Laravel 7 tiene dos modos:
     - Blog: pagina web que tiene el aspecto de un blog al uso.
     - Administración: panel para la gestión del blog. Creación de contenido, edición de posts...
     
## Herramientas
- Desarrollado en Laravel 7: php, blade, CSS, JQuery, HTML5...
- Plantilla adminLTE para el panel administración.

## Desarrollo

- Conexión a base de datos MySQL
- Integración con plantilla adminLTE para el panel admin





