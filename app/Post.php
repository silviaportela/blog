<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $dates = ['published_at']; //necesario par que lo trate como date, igual que created at

    //RELACION 1:N CON CATEGORIAS
    public function category($value='')
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        //un post puede tener MUCHOS tags
        //como hemos llamado tag_id al indice, no hay que hacer nada mas
        return $this->belongsToMany(Tag::class);
    }
}
